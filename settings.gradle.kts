rootProject.name = "Currency Calculator"
include(":app", ":domain", ":data", ":ui")