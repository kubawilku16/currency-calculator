object App {
    const val applicationId = "com.kuba.cc"
    const val versionCode = 1
    const val versionName = "1.0.0"

    const val compileSdk = 29
    const val buildToolSdk = "29.0.2"
    const val minSdk = 21
    const val targetSdk = 29
}

private object Versions {
    const val kotlinPlugin = "1.3.72"
    const val gradlePlugin = "4.0.1"

    const val kotlin = "1.3.72"
    const val supportLibrary = "1.0.0"
    const val recyclerView = "1.1.0"
    const val koin = "2.0.0"
    const val rxAndroid = "2.1.1"
    const val dataBinding = "3.0.1"
    const val glide = "4.11.0"
    const val circleImage = "3.1.0"
    const val retrofit = "2.9.0"
    const val lifecycle = "2.2.0"
}

object RootDependencies {
    const val gradle = "com.android.tools.build:gradle:${Versions.gradlePlugin}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinPlugin}"
}

object UiDependencies {
    const val supportV4 = "androidx.legacy:legacy-support-v4:${Versions.supportLibrary}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.supportLibrary}"
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val dataBinding = "com.android.databinding:compiler:${Versions.dataBinding}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideProcessor = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val circleImage = "de.hdodenhof:circleimageview:${Versions.circleImage}"
}

object CommonDependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val jacksonConverter = "com.squareup.retrofit2:converter-jackson:${Versions.retrofit}"
    const val retrofitRx2Adapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
}
