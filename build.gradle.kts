buildscript {
    repositories {
        google()
        jcenter() {
            content {
                includeGroup("org.jetbrains.kotlinx")
            }
        }
    }
    dependencies {
        classpath(RootDependencies.gradle)
        classpath(RootDependencies.kotlinGradle)
    }
}

plugins {
    id("io.gitlab.arturbosch.detekt").version("1.10.0")
}
subprojects {
    apply(mapOf("plugin" to "io.gitlab.arturbosch.detekt"))
    detekt {
        toolVersion = "1.10.0"
    }
}
detekt {
    failFast = true
    buildUponDefaultConfig = false
    config = files("$projectDir/config/detekt/detekt.yml")
    reports {
        html.enabled = true
        xml.enabled = true
        txt.enabled = true
    }
}
tasks {
    withType<io.gitlab.arturbosch.detekt.Detekt> {
        this.jvmTarget = "1.8"
    }
}
allprojects {
    repositories {
        google()
        jcenter()
    }
}
