package com.kuba.cc.ui.util

import androidx.annotation.DrawableRes
import com.kuba.cc.ui.R

class FlagUtils private constructor() {
    companion object {

        @DrawableRes
        fun getFlagByCurrency(currency: String): Int? {
            return flags[currency]
        }

        private val flags = mutableMapOf(
            "AUD" to R.drawable.australia_svg,
            "BGN" to R.drawable.bulgaria_svg,
            "BRL" to R.drawable.brazil_svg,
            "CAD" to R.drawable.canada_svg,
            "CHF" to R.drawable.switzerland_svg,
            "CNY" to R.drawable.china_svg,
            "CZK" to R.drawable.czech_republic_svg,
            "DKK" to R.drawable.denmark_svg,
            "EUR" to R.drawable.european_union_svg,
            "GBP" to R.drawable.united_kingdom_svg,
            "HKD" to R.drawable.hongkong_svg,
            "HRK" to R.drawable.croatia_svg,
            "HUF" to R.drawable.hungary_svg,
            "IDR" to R.drawable.indonesia_svg,
            "ILS" to R.drawable.israel_svg,
            "INR" to R.drawable.india_svg,
            "ISK" to R.drawable.iceland_svg,
            "JPY" to R.drawable.japan_svg,
            "KRW" to R.drawable.south_korea_svg,
            "MXN" to R.drawable.mexico_svg,
            "MYR" to R.drawable.malasya_svg,
            "NOK" to R.drawable.norway_svg,
            "NZD" to R.drawable.new_zealand_svg,
            "PHP" to R.drawable.philippines_svg,
            "PLN" to R.drawable.poland_svg,
            "RON" to R.drawable.romania_svg,
            "RUB" to R.drawable.russia_svg,
            "SEK" to R.drawable.sweden_svg,
            "SGD" to R.drawable.singapore_svg,
            "THB" to R.drawable.thailand_svg,
            "USD" to R.drawable.usa_svg,
            "ZAR" to R.drawable.south_africa_svg
        )
    }
}
