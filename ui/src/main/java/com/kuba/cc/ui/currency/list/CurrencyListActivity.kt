package com.kuba.cc.ui.currency.list

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kuba.cc.ui.R
import com.kuba.cc.ui.databinding.ActivityCurrencyListBinding
import kotlinx.android.synthetic.main.activity_currency_list.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class CurrencyListActivity : AppCompatActivity() {

    private lateinit var model: CurrencyListViewModel
    private var adapter: CurrencyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityCurrencyListBinding>(
            this,
            R.layout.activity_currency_list
        )
        model = getViewModel()
        binding.model = model
        model.init()
        initObservers()
        setContentView(binding.root)
        currencyRecycler.itemAnimator?.changeDuration = 0
        currencyRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                (recyclerView.layoutManager as? LinearLayoutManager)?.let {
                    if (it.findFirstVisibleItemPosition() > 0) {
                        val imm =
                            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(recyclerView.windowToken, 0)
                        recyclerView.isFocusable = false
                        recyclerView.isFocusableInTouchMode = false
                        recyclerView.isFocusable = true
                        recyclerView.isFocusableInTouchMode = true
                    }
                }
            }
        })
    }

    private fun initObservers() {
        model.rates.observe(this, Observer {
            if (adapter != null) {
                adapter!!.items[0].run {
                    val oldRate = this.rate
                    rate =
                        it.firstOrNull { item -> item.currency == this.currency }?.rate ?: oldRate
                    adapter!!.defaultCurrencyValue = adapter!!.defaultCurrencyValue * oldRate / rate
                }
                adapter!!.items.subList(1, adapter!!.itemCount - 1).forEachIndexed { pos, rate ->
                    rate.rate =
                        it.firstOrNull { item -> item.currency == rate.currency }?.rate ?: rate.rate
                    adapter!!.notifyItemChanged(pos + 1)
                }
            } else {
                adapter = CurrencyAdapter(it.toMutableList(), 0f) { pos ->
                    moveItem(pos)
                }
                currencyRecycler.adapter = adapter
                currencyRecycler.adapter!!.notifyDataSetChanged()
            }
        })
        model.lastRightReadTime.observe(this, Observer {
            currencyErrorText.visibility =
                if (it == null) View.GONE
                else {
                    currencyErrorText.text =
                        if (model.rates.value?.isNotEmpty() == true)
                            getString(R.string.currency_error_time, it)
                        else getString(R.string.currency_error_invoke)
                    View.VISIBLE
                }
            currencyTitle.visibility =
                if (model.rates.value?.isNotEmpty() == true) View.VISIBLE else View.GONE
            currencyErrorProgress.visibility = currencyErrorText.visibility
        })
    }

    private fun moveItem(position: Int) {
        currencyRecycler.stopScroll()
        if (currencyRecycler.isComputingLayout) {
            adapter?.let {
                currencyRecycler.post {
                    it.items.add(0, it.items.removeAt(position))
                    it.notifyItemMoved(position, 0)
                }
            }
        } else {
            adapter?.let {
                it.items.add(0, it.items.removeAt(position))
                it.notifyItemMoved(position, 0)
            }
        }
    }
}
