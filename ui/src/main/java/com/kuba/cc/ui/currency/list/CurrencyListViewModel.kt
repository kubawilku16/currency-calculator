package com.kuba.cc.ui.currency.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kuba.cc.domain.Repository
import com.kuba.cc.domain.entity.CurrencyRate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class CurrencyListViewModel(private val repository: Repository) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val _rates = MutableLiveData<List<CurrencyRate>>()

    val rates: LiveData<List<CurrencyRate>> = _rates
    val lastRightReadTime = MutableLiveData<String?>().apply { this.value = null }

    fun init() {
        start(
            repository.getRatesForCurrency(DEFAULT_CURRENCY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onGetCurrenciesSuccess(it) }, { onGetCurrenciesError() })
        )
    }

    private fun onGetCurrenciesSuccess(it: List<CurrencyRate>?) {
        lastRightReadTime.value = null
        _rates.value = it
    }

    private fun onGetCurrenciesError() {
        if (lastRightReadTime.value == null) {
            lastRightReadTime.value =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(
                    System.currentTimeMillis() - ONE_SECOND
                )
        }
        init()
    }

    private fun start(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    companion object {
        private const val DEFAULT_CURRENCY = "EUR"
        private const val ONE_SECOND = 1000L
    }
}
