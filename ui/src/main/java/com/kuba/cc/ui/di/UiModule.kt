package com.kuba.cc.ui.di

import com.kuba.cc.ui.currency.list.CurrencyListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewModelsModule = module {
    viewModel { CurrencyListViewModel(get()) }
}
