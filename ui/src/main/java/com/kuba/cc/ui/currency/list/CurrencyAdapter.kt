package com.kuba.cc.ui.currency.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kuba.cc.domain.entity.CurrencyRate
import com.kuba.cc.ui.R
import com.kuba.cc.ui.util.FlagUtils
import com.kuba.cc.ui.util.SimpleTextWatcher
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_currency_item.*

class CurrencyAdapter(
    val items: MutableList<CurrencyRate>,
    var defaultCurrencyValue: Float,
    private val onItemFocused: (Int) -> Unit
) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_currency_item, parent, false)
        )
    }

    override fun getItemCount() = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        var textWatcher: SimpleTextWatcher? = null

        fun bind(position: Int) {
            items[position].run {
                Glide
                    .with(containerView)
                    .load(FlagUtils.getFlagByCurrency(currency))
                    .placeholder(R.drawable.flag_unknown)
                    .centerCrop()
                    .into(currencyItemFlag)
                currencyItemTitle.text = currency
                currencyItemSubtitle.text = name
                val value = defaultCurrencyValue * rate
                currencyItemInput.setText(
                    if (value != 0f) containerView.context.getString(
                        R.string.input_2_decimal,
                        defaultCurrencyValue * rate
                    )
                    else ""
                )
                currencyItemInput.setOnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) onItemFocused.invoke(position)
                }
                textWatcher = SimpleTextWatcher {
                    (it.replace(",", ".").toFloatOrNull() ?: ZERO).let { value ->
                        if (items[0] == this) {
                            defaultCurrencyValue = value / rate
                            notifyItemRangeChanged(1, itemCount - 1)
                        }
                    }
                }
                currencyItemInput.addTextChangedListener(textWatcher!!)
            }
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.currencyItemInput.removeTextChangedListener(holder.textWatcher)
    }

    companion object {
        private const val ZERO = 0.0F
    }
}
