package com.kuba.cc.ui.util

import android.text.Editable
import android.text.TextWatcher

class SimpleTextWatcher(private val onTextChange: (String) -> Unit) : TextWatcher{
    override fun afterTextChanged(s: Editable?) {
        // ignored
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        // ignored
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        onTextChange.invoke(s.toString())
    }
}
