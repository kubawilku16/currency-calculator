plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}
android {
    compileSdkVersion(App.compileSdk)
    buildToolsVersion = App.buildToolSdk

    defaultConfig {
        minSdkVersion(App.minSdk)
        targetSdkVersion(App.targetSdk)
        multiDexEnabled = false
    }

    buildFeatures.dataBinding = true

    androidExtensions {
        isExperimental = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
}

dependencies {
    implementation(project(":domain"))
    implementation(CommonDependencies.kotlin)
    implementation(CommonDependencies.kotlinReflect)
    implementation(CommonDependencies.koinViewModel)
    implementation(CommonDependencies.rxAndroid)

    implementation(UiDependencies.appCompat)
    implementation(UiDependencies.supportV4)
    implementation(UiDependencies.viewModel)
    implementation(UiDependencies.liveData)
    implementation(UiDependencies.recyclerView)
    implementation(UiDependencies.dataBinding)
    implementation(UiDependencies.glide)
    kapt(UiDependencies.glideProcessor)
    implementation(UiDependencies.circleImage)
}