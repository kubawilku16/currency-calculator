package com.kuba.cc.domain.entity

data class CurrencyRate(val currency: String, val name: String, var rate: Float)
