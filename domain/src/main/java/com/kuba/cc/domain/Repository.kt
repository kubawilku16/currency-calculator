package com.kuba.cc.domain

import com.kuba.cc.domain.entity.CurrencyRate
import io.reactivex.Observable

/**
 * Connector between data source and ViewModel
 */
interface Repository {

    /**
     *  Provide rates for selected currency each second
     *
     *  @param currency - current selected currency
     */
    fun getRatesForCurrency(currency: String): Observable<List<CurrencyRate>>
}
