package com.kuba.cc

import android.app.Application
import com.kuba.cc.di.AppModule
import com.kuba.cc.ui.di.ViewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(AppModule, ViewModelsModule)
        }
    }
}
