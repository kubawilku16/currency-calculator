package com.kuba.cc.di

import com.kuba.cc.BuildConfig
import com.kuba.cc.data.ApiService
import com.kuba.cc.data.RepositoryImpl
import com.kuba.cc.domain.Repository
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

val AppModule = module {
    single<Repository> { RepositoryImpl(get()) }

    single {
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create())
            .client(get())
            .baseUrl(BuildConfig.API_URL)
            .build()
            .create(ApiService::class.java)
    }

    single {
        val timeout = 1L
        OkHttpClient()
            .newBuilder()
            .callTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .build()
    }
}
