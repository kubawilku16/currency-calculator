plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
}

apply(from = "../ktlint.gradle")

android {
    compileSdkVersion(App.compileSdk)
    buildToolsVersion = App.buildToolSdk

    defaultConfig {
        applicationId = App.applicationId
        minSdkVersion(App.minSdk)
        targetSdkVersion(App.targetSdk)
        versionCode = App.versionCode
        versionName = App.versionName
        multiDexEnabled = false
    }

    buildFeatures.dataBinding = true

    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }

    buildTypes.all {
        buildConfigField("String", "API_URL", "\"https://hiring.revolut.codes/api/android/\"")
    }

    buildTypes {
        getByName("release"){
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug"){
            isMinifyEnabled = false
        }
    }
}

dependencies {
    implementation(project(":ui"))
    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(CommonDependencies.kotlin)
    implementation(CommonDependencies.koin)
    implementation(CommonDependencies.koinViewModel)
    implementation(CommonDependencies.retrofit)
    implementation(CommonDependencies.jacksonConverter)
    implementation(CommonDependencies.retrofitRx2Adapter)
}
