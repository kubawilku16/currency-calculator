plugins{
    id("com.android.library")
    kotlin("android")
}

apply(from = "../ktlint.gradle")

android {
    compileSdkVersion(App.compileSdk)
    buildToolsVersion = App.buildToolSdk

    defaultConfig {
        minSdkVersion(App.minSdk)
        targetSdkVersion(App.targetSdk)
    }

    buildTypes {
        getByName("release"){
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(project(":domain"))
    implementation(CommonDependencies.kotlin)
    implementation(CommonDependencies.kotlinReflect)
    implementation(CommonDependencies.koin)
    implementation(CommonDependencies.rxAndroid)
    implementation(CommonDependencies.retrofit)
}
