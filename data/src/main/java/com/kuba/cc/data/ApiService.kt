package com.kuba.cc.data

import com.kuba.cc.data.response.CurrencyResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Delegate to communicate with server API
 */
interface ApiService {

    /**
     * Requests server for rates for specific [currency]
     *
     * @param currency
     */
    @GET("latest")
    fun getRatesForCurrency(
        @Query(value = "base", encoded = false) currency: String
    ): Single<CurrencyResponse>
}
