package com.kuba.cc.data

import com.kuba.cc.domain.Repository
import com.kuba.cc.domain.entity.CurrencyRate
import io.reactivex.Observable
import java.util.Currency
import java.util.concurrent.TimeUnit

class RepositoryImpl(private val apiService: ApiService) : Repository {
    override fun getRatesForCurrency(currency: String): Observable<List<CurrencyRate>> {
        return apiService.getRatesForCurrency(currency)
            .map {
                listOf(
                    listOf(createCurrencyRate(it.baseCurrency, 1.0f)),
                    it.rates.map { (currency, rate) -> createCurrencyRate(currency, rate) }
                ).flatten()
            }
            .delay(1L, TimeUnit.SECONDS)
            .repeat()
            .toObservable()
    }

    private fun createCurrencyRate(currency: String, rate: Float): CurrencyRate {
        return CurrencyRate(currency, Currency.getInstance(currency).displayName, rate)
    }
}
