package com.kuba.cc.data.response

data class CurrencyResponse(val baseCurrency: String = "", val rates: Map<String, Float> = mapOf())
